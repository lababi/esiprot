from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [])

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('esiprot_py3.py', base=base, icon='esiprot.ico')
]

setup(name='ESIProt',
      version = '1.1',
      description = 'Deconvolution and molecular weight calculation of proteins from electrospray ionization (ESI) data.',
      options = dict(build_exe = buildOptions),
      executables = executables)
