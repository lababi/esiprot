from cx_Freeze import setup, Executable
import sys
import os
import tkinter

# Dependencies are automatically detected, but it might need
# fine tuning.

os.environ['TCL_LIBRARY'] = "C:\\Python36-32\\tcl\\tcl8.6"
os.environ['TK_LIBRARY'] = "C:\\Python36-32\\tcl\\tk8.6"

files = {"include_files": ["C:\\Python36-32\\vcruntime140.dll", "C:\\Python36-32\\DLLs\\tcl86t.dll", "C:\\Python36-32\\DLLs\\tk86t.dll", "C:\\Python36-32\\tcl\\tk8.6", "C:\\Python36-32\\tcl\\tcl8.6"], "packages": ["tkinter"]}

base = 'Win32GUI' if sys.platform=='win32' else None


executables = [
    Executable('esiprot_py3.py', base=base, icon='esiprot.ico')
]

setup(name='ESIProt',
      version = '1.1',
      description = 'Deconvolution and molecular weight calculation of proteins from electrospray ionization (ESI) data.',
      options = {"build_exe": files},
      executables = executables)
